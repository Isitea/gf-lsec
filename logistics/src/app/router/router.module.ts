import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Angular Router Module import
import { Routes, RouterModule } from "@angular/router";

// Component import
import { LogisticsComponent } from '../logistics/logistics.component';

const routers: Routes = [
  { path: '', component: LogisticsComponent }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot( routers )
  ],
  declarations: [],
  exports: [ RouterModule ]
})
export class AppRouterModule { }
