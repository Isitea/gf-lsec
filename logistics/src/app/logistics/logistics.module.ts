import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogisticsComponent } from './logistics.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [LogisticsComponent]
})
export class LogisticsModule { }
